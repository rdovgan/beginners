package com.asd.entities;

import com.asd.constants.ArchivePriceState;
import com.asd.constants.ArchivePriceType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ArchivePrice {
	private Integer id;
	private Integer entityId;
	private String entityType;
	private String name;
	private ArchivePriceState state;
	private ArchivePriceType type;
	private Double value;
	private LocalDateTime version;
}
