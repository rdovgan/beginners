package com.asd.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ReservationConfirmation {
	private Integer id;
	private Integer reservationId;
	private Integer channelPartnerId;
	private String confirmationId;
	private LocalDateTime createdTime;
	private LocalDateTime version;
}