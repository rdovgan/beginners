package com.asd.entities;

import com.asd.constants.Currency;
import com.asd.constants.ReservationState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {
	private Integer id;
	private Integer organizationId;
	private Integer agentId;
	private Integer customerId;
	private Integer productId;
	private ReservationState state;
	private LocalDateTime fromDate;
	private LocalDateTime toDate;
	private Double price;
	private Double quote;
	private Currency currency;
	private Integer guestNumber;
	private String notes;
	private LocalDateTime version;
}