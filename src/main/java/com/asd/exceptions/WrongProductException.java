package com.asd.exceptions;

public class WrongProductException extends Exception {

	public WrongProductException(String message) {
		super(message);
	}

}
