package com.asd.dto;

import com.asd.adapters.LocalDateTimeAdapter;
import com.asd.constants.ArchivePriceState;
import com.asd.constants.ArchivePriceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ArchivePriceDto {
	private Integer id;
	private Integer entityId;
	private String entityType;
	private String name;
	private ArchivePriceState state;
	private ArchivePriceType type;
	private Double value;

	@XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
	private LocalDateTime version;
}