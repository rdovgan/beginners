package com.asd.dto;

import com.asd.adapters.LocalDateTimeAdapter;
import com.asd.constants.Currency;
import com.asd.constants.ReservationState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ReservationDto {
	private Integer id;
	private Integer organizationId;
	private Integer agentId;
	private Integer customerId;
	private Integer productId;
	private ReservationState state;

	@XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
	private LocalDateTime fromDate;

	@XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
	private LocalDateTime toDate;
	private Double price;
	private Double quote;
	private Currency currency;
	private Integer guestNumber;
	private String notes;

	@XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
	private LocalDateTime version;
}
