package com.asd.dto;

import com.asd.constants.ArchivePriceType;
import com.asd.entities.Reservation;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class ReservationArchivePriceTypeMapDto {
	private Reservation reservation;
	private Map<ArchivePriceType, List<ArchivePriceDto>> typePriceMap;
}