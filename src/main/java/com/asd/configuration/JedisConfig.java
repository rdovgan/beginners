package com.asd.configuration;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import redis.clients.jedis.Jedis;

@Configuration
@PropertySource("/config/redis.properties")
public class JedisConfig {

	@Value("${redis.host}")
	private String host;

	@Value("${redis.port}")
	private Integer port;

	@Value("${redis.password}")
	private String password;

	@Bean
	public Jedis getJedis() {
		Jedis jedis = new Jedis(host, port);
		jedis.auth(password);
		return jedis;
	}

	@Bean
	public Gson getGson() {
		return new Gson();
	}
}