package com.asd.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = "com.asd.mapper")
@SpringBootApplication(scanBasePackages = "com.asd")
public class WebConfig {
	public static void main(String[] args) {
		SpringApplication.run(WebConfig.class, args);
	}
}
