package com.asd.services;

import com.asd.dto.TaxDto;

import java.util.List;

public interface IsTaxService {

	void create(TaxDto taxToCreate);

	TaxDto readById(Integer id);

	void update(TaxDto tax);

	List<TaxDto> readByProductId(Integer productId);

	List<TaxDto> readByProductIdAndState(Integer productId, String state);

	void changeTaxStateToFinal(String productId);

	List<TaxDto> getTaxByIds(List<Integer> taxIds);

	void insertList(List<TaxDto> itemsToInsert);

	void updateList(List<TaxDto> itemsToUpdate);

}
