package com.asd.services;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Getter
@Service
@RequiredArgsConstructor
public class RedisService {
	private final Jedis jedis;
	private final Gson gson;

	public boolean existsByKey(String combineKey) {
		return jedis.exists(combineKey);
	}

	public <T> boolean existsByKey(Integer key, Class<T> clazz) {
		return existsByKey(getCombineKeyForRedis(key, clazz));
	}

	public <T> T read(Integer id, Class<T> clazz) {
		return gson.fromJson(jedis.get(getCombineKeyForRedis(id, clazz)), clazz);
	}

	public <T> void push(Object data, Class<T> clazz, Integer id) {
		jedis.setex(getCombineKeyForRedis(id, clazz), 120, gson.toJson(data, clazz));
	}

	public <T> String getCombineKeyForRedis(Integer id, Class<T> clazz) {
		return String.format("%s.%s", clazz.getName(), id);
	}
}