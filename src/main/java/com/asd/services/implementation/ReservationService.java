package com.asd.services.implementation;

import com.asd.constants.EntityType;
import com.asd.constants.ReservationState;
import com.asd.database.DatabaseService;
import com.asd.dto.ReservationArchivePriceTypeMapDto;
import com.asd.dto.ReservationDto;
import com.asd.entities.Reservation;
import com.asd.exceptions.WrongArgumentException;
import com.asd.mapper.ReservationMapper;
import com.asd.services.IsReservationService;
import com.asd.services.MailService;
import com.asd.services.RedisService;
import com.asd.utils.ReservationConverter;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReservationService implements IsReservationService {

	private final ArchivePriceService archivePriceService;
	private final MailService mailService;
	private final RedisService redisService;
	private final ReservationConverter reservationConverter;

	@Override
	public void create(ReservationDto reservationDto) {
		if (reservationDto == null || reservationDto.getProductId() == null) {
			throw new WrongArgumentException("Invalid object to create");
		}
		Reservation entity = reservationConverter.convertToEntity(reservationDto);
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			mapper.create(entity);
			entity = mapper.readById(entity.getId());
			session.commit();
		}
		redisService.push(entity, Reservation.class, entity.getId());
	}

	@Override
	public void update(ReservationDto reservationDto) {
		if (reservationDto == null || reservationDto.getProductId() == null) {
			throw new WrongArgumentException("Invalid object to update");
		}
		Reservation entity = reservationConverter.convertToEntity(reservationDto);
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			mapper.update(entity);
			session.commit();
		}
		redisService.push(entity, Reservation.class, entity.getId());
	}

	@Override
	public ReservationDto readById(Integer id) {
		if (id == null) {
			throw new WrongArgumentException("Id to read cannot be null");
		}
		if (id <= 0) {
			throw new WrongArgumentException("Id should be positive");
		}
		if (redisService.existsByKey(id, Reservation.class)) {
			return reservationConverter.convertToDto(redisService.read(id, Reservation.class));
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			Reservation entity = mapper.readById(id);
			redisService.push(entity, Reservation.class, id);
			return reservationConverter.convertToDto(entity);
		}
	}

	@Override
	public void changeState(Integer id, String state) {
		if (id == null) {
			throw new WrongArgumentException("Id to change state cannot be null");
		}
		if (id <= 0) {
			throw new WrongArgumentException("Id should be positive");
		}
		if (state == null || !EnumUtils.isValidEnum(ReservationState.class, state)) {
			throw new WrongArgumentException("Invalid state to change");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			mapper.changeState(id, ReservationState.valueOf(state));
			redisService.push(mapper.readById(id), Reservation.class, id);
			session.commit();
		}
	}

	@Override
	public void changeStateToClosed(Integer id) {
		if (id == null) {
			throw new WrongArgumentException("Id to change state to closed cannot be null");
		}
		if (id <= 0) {
			throw new WrongArgumentException("Id should be positive");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			mapper.changeStateToClosed(id);
			redisService.push(mapper.readById(id), Reservation.class, id);
			session.commit();
		}
	}

	@Override
	public void insertList(List<ReservationDto> itemsToInsert) {
		if (itemsToInsert == null) {
			throw new WrongArgumentException("Invalid list of items to insert");
		}
		List<ReservationDto> filteredItems;
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			filteredItems = itemsToInsert.stream()
					.filter(item -> item.getProductId() != null)
					.collect(Collectors.toList());
			mapper.insertList(reservationConverter.convertToEntityList(filteredItems));
			session.commit();
		}
		if (filteredItems.size() < itemsToInsert.size()) {
			throw new WrongArgumentException(String.format("%d items were inserted only of %d", filteredItems.size(), itemsToInsert.size()));
		}
	}

	@Override
	public List<ReservationDto> findGreaterThanDateAndQuote(LocalDateTime fromDate, Integer quote, Integer limit) {
		if (fromDate != null) {
			System.out.println(fromDate);
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			return reservationConverter.convertToDtoList(mapper.findGreaterThanDateAndQuote(fromDate, quote, limit));
		}
	}

	@Override
	public void updateState(Integer productId, LocalDateTime fromDate, LocalDateTime toDate, String newState) {
		if (newState == null || !EnumUtils.isValidEnum(ReservationState.class, newState)) {
			throw new WrongArgumentException("Invalid state to update");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			mapper.updateState(productId, fromDate, toDate, ReservationState.valueOf(newState));
			session.commit();
		}
	}

	@Override
	public List<ReservationDto> readByCustomerId(Integer customerId, Integer start, Integer limit) {
		if (customerId == null) {
			throw new WrongArgumentException("Customer id to read cannot be null");
		}
		if (customerId <= 0) {
			throw new WrongArgumentException("Customer id should be positive");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			return reservationConverter.convertToDtoList(mapper.readByCustomerId(customerId, start, limit));
		}
	}

	@Override
	public List<ReservationDto> readByDateRange(LocalDateTime startDate, LocalDateTime endDate) {
		if (startDate == null || endDate == null) {
			throw new WrongArgumentException("Invalid date range to read");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			return reservationConverter.convertToDtoList(mapper.readByDateRange(startDate, endDate));
		}
	}

	@Override
	public ReservationArchivePriceTypeMapDto getReservationAndGroupArchivePriceByTypeAndSortByVersion(Integer id, Integer limit) {
		ReservationArchivePriceTypeMapDto mapDto;
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			mapDto = ReservationArchivePriceTypeMapDto.builder()
					.reservation(mapper.readById(id))
					.typePriceMap(archivePriceService.groupByTypeAndSortedByVersion(EntityType.Reservation.name(), id, limit))
					.build();
		}
		try {
			mailService.sendWebPage("test@mail.com", "Report test", "report.ftl", mapDto);
		} catch (MessagingException | TemplateException | IOException e) {
			e.printStackTrace();
		}
		return mapDto;
	}

	@Override
	public List<ReservationDto> findByConfirmationIdPattern(String pattern) {
		if (StringUtils.isEmpty(pattern)) {
			throw new WrongArgumentException("Invalid confirmation id pattern");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationMapper mapper = session.getMapper(ReservationMapper.class);
			return reservationConverter.convertToDtoList(mapper.findByConfirmationIdPattern(pattern));
		}
	}
}