package com.asd.services.implementation;

import com.asd.database.DatabaseService;
import com.asd.dto.ReservationConfirmationDto;
import com.asd.entities.ReservationConfirmation;
import com.asd.exceptions.WrongArgumentException;
import com.asd.mapper.ReservationConfirmationMapper;
import com.asd.services.IsReservationConfirmationService;
import com.asd.services.RedisService;
import com.asd.utils.ReservationConfirmationConverter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReservationConfirmationService implements IsReservationConfirmationService {
	private final ReservationConfirmationConverter converter;
	private final RedisService redisService;

	@Override
	public void create(ReservationConfirmationDto resConfirmDto) {
		if (resConfirmDto == null) {
			throw new WrongArgumentException("Reservation confirmation for create cannot be null");
		}
		if (resConfirmDto.getReservationId() == null) {
			throw new WrongArgumentException("Reservation confirmation's reservationId cannot be null");
		}
		ReservationConfirmation entity = converter.toEntity(resConfirmDto);
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationConfirmationMapper mapper = session.getMapper(ReservationConfirmationMapper.class);
			mapper.create(entity);
			entity = mapper.readById(entity.getId());
			session.commit();
		}
		redisService.push(entity, ReservationConfirmation.class, entity.getId());
	}

	@Override
	public void update(ReservationConfirmationDto resConfirmDto) {
		if (resConfirmDto == null) {
			throw new WrongArgumentException("Reservation confirmation for update cannot be null");
		}
		if (resConfirmDto.getReservationId() == null) {
			throw new WrongArgumentException("Reservation confirmation's reservationId cannot be null");
		}
		ReservationConfirmation entity = converter.toEntity(resConfirmDto);
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationConfirmationMapper mapper = session.getMapper(ReservationConfirmationMapper.class);
			mapper.update(entity);
			session.commit();
		}
		redisService.push(entity, ReservationConfirmation.class, entity.getId());
	}

	@Override
	public ReservationConfirmationDto readById(Integer id) {
		if (id == null) {
			throw new WrongArgumentException("Id cannot be null");
		}
		if (redisService.existsByKey(id, ReservationConfirmation.class)) {
			return converter.toDto(redisService.read(id, ReservationConfirmation.class));
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationConfirmationMapper mapper = session.getMapper(ReservationConfirmationMapper.class);
			ReservationConfirmation entity = mapper.readById(id);
			redisService.push(entity, ReservationConfirmation.class, id);
			return converter.toDto(entity);
		}
	}

	@Override
	public void insertList(List<ReservationConfirmationDto> itemsToInsert) {
		if (CollectionUtils.isEmpty(itemsToInsert)) {
			throw new WrongArgumentException("Archive price's list for insert was empty or null. Inserted: 0");
		}
		List<ReservationConfirmationDto> filteredList = itemsToInsert.stream()
				.filter(Objects::nonNull)
				.filter(e -> e.getReservationId() != null)
				.collect(Collectors.toList());
		try (SqlSession session = DatabaseService.openSession()) {
			ReservationConfirmationMapper mapper = session.getMapper(ReservationConfirmationMapper.class);
			mapper.insertList(converter.toEntity(filteredList));
			session.commit();
		}
		if (filteredList.size() != itemsToInsert.size()) {
			throw new WrongArgumentException(String.format("Must be inserted: %d, but was: %d", itemsToInsert.size(), filteredList.size()));
		}
	}
}
