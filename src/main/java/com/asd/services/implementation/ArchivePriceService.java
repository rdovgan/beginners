package com.asd.services.implementation;

import com.asd.constants.ArchivePriceState;
import com.asd.constants.ArchivePriceType;
import com.asd.constants.EntityType;
import com.asd.database.DatabaseService;
import com.asd.dto.ArchivePriceDto;
import com.asd.entities.ArchivePrice;
import com.asd.exceptions.WrongArgumentException;
import com.asd.mapper.ArchivePriceMapper;
import com.asd.services.IsArchivePriceService;
import com.asd.services.RedisService;
import com.asd.utils.ArchivePriceConverter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ArchivePriceService implements IsArchivePriceService {
	private final ArchivePriceConverter converter;
	private final RedisService redisService;

	@Override
	public void create(ArchivePriceDto priceToCreate) {
		if (priceToCreate == null) {
			throw new WrongArgumentException("Archive price for create cannot be null");
		}
		ArchivePrice entity = converter.toEntity(priceToCreate);
		try (SqlSession session = DatabaseService.openSession()) {
			ArchivePriceMapper mapper = session.getMapper(ArchivePriceMapper.class);
			mapper.create(entity);
			entity = mapper.read(entity.getId());
			session.commit();
		}
		redisService.push(entity, ArchivePrice.class, entity.getId());
	}

	@Override
	public ArchivePriceDto read(Integer id) {
		if (id == null) {
			throw new WrongArgumentException("Id cannot be null");
		}
		if (redisService.existsByKey(id, ArchivePrice.class)) {
			return converter.toDto(redisService.read(id, ArchivePrice.class));
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ArchivePriceMapper mapper = session.getMapper(ArchivePriceMapper.class);
			ArchivePrice archivePrice = mapper.read(id);
			redisService.push(archivePrice, ArchivePrice.class, archivePrice.getId());
			return converter.toDto(archivePrice);
		}
	}

	@Override
	public void update(ArchivePriceDto price) {
		if (price == null) {
			throw new WrongArgumentException("Archive price for create cannot be null");
		}
		ArchivePrice entity = converter.toEntity(price);
		try (SqlSession session = DatabaseService.openSession()) {
			ArchivePriceMapper mapper = session.getMapper(ArchivePriceMapper.class);
			mapper.update(entity);
			entity = mapper.read(entity.getId());
			session.commit();
		}
		redisService.push(entity, ArchivePrice.class, entity.getId());
	}

	@Override
	public void changeState(Integer id, String state) {
		if (id == null) {
			throw new WrongArgumentException("Id cannot be null");
		}
		if (state != null && !EnumUtils.isValidEnum(ArchivePriceState.class, state)) {
			throw new WrongArgumentException("Not valid ArchivePriceState");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ArchivePriceMapper mapper = session.getMapper(ArchivePriceMapper.class);
			mapper.changeState(id, ArchivePriceState.valueOf(state));
			redisService.push(mapper.read(id), ArchivePrice.class, id);
			session.commit();
		}
	}

	@Override
	public Map<ArchivePriceType, List<ArchivePriceDto>> groupByTypeAndSortedByVersion(String entityType, Integer entityId, Integer limit) {
		if (limit != null && limit < 0) {
			throw new WrongArgumentException("Limit cannot be less then 0");
		}
		if (entityType != null && !EnumUtils.isValidEnum(EntityType.class, entityType)) {
			throw new WrongArgumentException("Not valid Entity");
		}
		try (SqlSession session = DatabaseService.openSession()) {
			ArchivePriceMapper mapper = session.getMapper(ArchivePriceMapper.class);
			return converter.toDto(mapper.groupByTypeAndSortedByVersion(entityType, entityId, limit))
					.stream()
					.collect(Collectors.groupingBy(ArchivePriceDto::getType));
		}
	}

	@Override
	public void insertList(List<ArchivePriceDto> itemsToInsert) {
		if (CollectionUtils.isEmpty(itemsToInsert)) {
			throw new WrongArgumentException("Archive price's list for insert was empty or null. Inserted: 0");
		}
		List<ArchivePriceDto> filteredList = itemsToInsert.stream()
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		try (SqlSession session = DatabaseService.openSession()) {
			ArchivePriceMapper mapper = session.getMapper(ArchivePriceMapper.class);
			mapper.insertList(converter.toEntity(filteredList));
			session.commit();
		}
		if (filteredList.size() != itemsToInsert.size()) {
			throw new WrongArgumentException(String.format("Must be inserted: %d, but was: %d", itemsToInsert.size(), filteredList.size()));
		}
	}
}