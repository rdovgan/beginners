package com.asd.services;

import com.asd.entities.IsPerson;

import javax.annotation.Nonnull;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;

public interface IsPersonService {

	/**
	 * Filters persons by it's name. If persons list is empty or null, returns an empty list. Also, items without age should be ignored.
	 * If prefix wasn't provided, return personList without filtering.
	 *
	 * @param personList list of persons.
	 * @param prefix value to find at the start of person name.
	 * @return list of persons where it's name starts with provided value.
	 */
	@Nonnull
	List<IsPerson> collectPersonsWithNameStartsWith(List<IsPerson> personList, String prefix);

	/**
	 * Groups persons by age. Returns an empty map if no person was provided. Filters out persons with wrong age (negative or null)
	 *
	 * @param personList with person objects.
	 * @return a map with persons as value and age value as a key.
	 */
	@Nonnull
	Map<Integer, List<IsPerson>> collectPersonsByAge(List<IsPerson> personList);

	/**
	 * Calculates an average age for provided persons.
	 *
	 * @param personList with person objects.
	 * @return average age amount or zero if no correct person was provided.
	 */
	@Nonnull
	Double calculateAverageAge(List<IsPerson> personList);

	/**
	 * Collect statistic for provided persons list.
	 *
	 * @param personList with person objects.
	 * @return sum and count statistic for persons list.
	 */
	@Nonnull
	IntSummaryStatistics sumAndCountAge(List<IsPerson> personList);

}
