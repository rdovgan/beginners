package com.asd.services;

import com.asd.dto.ReservationArchivePriceTypeMapDto;
import com.asd.dto.ReservationDto;

import java.time.LocalDateTime;
import java.util.List;

public interface IsReservationService {

	void create(ReservationDto reservationDto);

	void update(ReservationDto reservationDto);

	ReservationDto readById(Integer id);

	void changeState(Integer id, String state);

	void changeStateToClosed(Integer id);

	void insertList(List<ReservationDto> itemsToInsert);

	List<ReservationDto> findGreaterThanDateAndQuote(LocalDateTime fromDate, Integer quote, Integer limit);

	void updateState(Integer productId, LocalDateTime fromDate, LocalDateTime toDate, String newState);

	List<ReservationDto> readByCustomerId(Integer customerId, Integer start, Integer limit);

	List<ReservationDto> readByDateRange(LocalDateTime startDate, LocalDateTime endDate);

	List<ReservationDto> findByConfirmationIdPattern(String pattern);

	ReservationArchivePriceTypeMapDto getReservationAndGroupArchivePriceByTypeAndSortByVersion(Integer id, Integer limit);
}
