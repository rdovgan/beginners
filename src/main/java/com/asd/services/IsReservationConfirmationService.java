package com.asd.services;

import com.asd.dto.ReservationConfirmationDto;

import java.util.List;

public interface IsReservationConfirmationService {

	void create(ReservationConfirmationDto resConfirmDto);

	void update(ReservationConfirmationDto resConfirmDto);

	ReservationConfirmationDto readById(Integer id);

	void insertList(List<ReservationConfirmationDto> itemsToInsert);
}