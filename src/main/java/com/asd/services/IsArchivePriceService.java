package com.asd.services;

import com.asd.constants.ArchivePriceType;
import com.asd.dto.ArchivePriceDto;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * beginners.IsArchivePrice
 *
 * @Autor: art
 * @DateTime: 17.06.2021|15:18
 * @Version IsArchivePrice: 1.0
 * @pROJECT beginners
 */
public interface IsArchivePriceService {

	void create(ArchivePriceDto priceToCreate);

	ArchivePriceDto read(Integer id);

	void update(ArchivePriceDto price);

	void changeState(Integer id, String state);

	void insertList(List<ArchivePriceDto> itemsToInsert);

	Map<ArchivePriceType, List<ArchivePriceDto>> groupByTypeAndSortedByVersion(String entityType, Integer entityId, Integer limit);
}