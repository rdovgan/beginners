package com.asd.constants;

public enum ArchivePriceType {

	YIELD, INFO, FEE, TAX, COMMISSION, PRICE_RATE

}
