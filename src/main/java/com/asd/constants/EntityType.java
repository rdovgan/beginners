package com.asd.constants;

/**
 * Created by IntelliJ IDEA.
 * beginners.EntityType
 *
 * @Autor: art
 * @DateTime: 22.06.2021|19:46
 * @Version EntityType: 1.0
 * @pROJECT beginners
 */
public enum EntityType {
	Reservation, Archive_Price
}