package com.asd.typehandlers;

import com.asd.constants.Currency;
import org.apache.commons.lang3.EnumUtils;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes({ Currency.class })
public class CurrencyHandler implements TypeHandler<Currency> {
	@Override
	public void setParameter(PreparedStatement preparedStatement, int i, Currency currency, JdbcType jdbcType) throws SQLException {
		if (currency == null) {
			preparedStatement.setNull(i, jdbcType.TYPE_CODE);
		} else {
			preparedStatement.setString(i, currency.name());
		}
	}

	@Override
	public Currency getResult(ResultSet resultSet, String s) throws SQLException {
		return EnumUtils.isValidEnum(Currency.class, resultSet.getString(s)) ? Currency.valueOf(resultSet.getString(s)) : null;
	}

	@Override
	public Currency getResult(ResultSet resultSet, int i) throws SQLException {
		return EnumUtils.isValidEnum(Currency.class, resultSet.getString(i)) ? Currency.valueOf(resultSet.getString(i)) : null;
	}

	@Override
	public Currency getResult(CallableStatement callableStatement, int i) throws SQLException {
		return EnumUtils.isValidEnum(Currency.class, callableStatement.getString(i)) ? Currency.valueOf(callableStatement.getString(i)) : null;
	}
}