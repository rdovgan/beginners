package com.asd.typehandlers;

import com.asd.constants.ArchivePriceState;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
public class ArchivePriceStateTypeHandler extends BaseTypeHandler<ArchivePriceState> {
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, ArchivePriceState priceState, JdbcType jdbcType) throws SQLException {
		preparedStatement.setString(i, priceState.name());
	}

	@Override
	public ArchivePriceState getNullableResult(ResultSet resultSet, String s) throws SQLException {
		String stateFromDb = resultSet.getString(s);
		for (ArchivePriceState state : ArchivePriceState.values()) {
			if (state.name().equals(stateFromDb)) {
				return state;
			}
		}
		return null;
	}

	@Override
	public ArchivePriceState getNullableResult(ResultSet resultSet, int i) throws SQLException {
		String stateFromDb = resultSet.getString(i);
		for (ArchivePriceState state : ArchivePriceState.values()) {
			if (state.name().equals(stateFromDb)) {
				return state;
			}
		}
		return null;
	}

	@Override
	public ArchivePriceState getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
		String stateFromDb = callableStatement.getString(i);
		for (ArchivePriceState state : ArchivePriceState.values()) {
			if (state.name().equals(stateFromDb)) {
				return state;
			}
		}
		return null;
	}
}
