package com.asd.typehandlers;

import com.asd.constants.ArchivePriceType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
public class ArchivePriceTypeTypeHandler extends BaseTypeHandler<ArchivePriceType> {
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, ArchivePriceType priceType, JdbcType jdbcType) throws SQLException {
		preparedStatement.setString(i, priceType.name());
	}

	@Override
	public ArchivePriceType getNullableResult(ResultSet resultSet, String s) throws SQLException {
		String typeFromDb = resultSet.getString(s);
		for (ArchivePriceType type : ArchivePriceType.values()) {
			if (type.name().equals(typeFromDb)) {
				return type;
			}
		}
		return null;
	}

	@Override
	public ArchivePriceType getNullableResult(ResultSet resultSet, int i) throws SQLException {
		String typeFromDb = resultSet.getString(i);
		for (ArchivePriceType type : ArchivePriceType.values()) {
			if (type.name().equals(typeFromDb)) {
				return type;
			}
		}
		return null;
	}

	@Override
	public ArchivePriceType getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
		String typeFromDb = callableStatement.getString(i);
		for (ArchivePriceType type : ArchivePriceType.values()) {
			if (type.name().equals(typeFromDb)) {
				return type;
			}
		}
		return null;
	}
}
