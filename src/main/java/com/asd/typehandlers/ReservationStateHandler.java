package com.asd.typehandlers;

import com.asd.constants.ReservationState;
import org.apache.commons.lang3.EnumUtils;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes({ ReservationState.class })
public class ReservationStateHandler implements TypeHandler<ReservationState> {

	@Override
	public void setParameter(PreparedStatement preparedStatement, int i, ReservationState reservationState, JdbcType jdbcType) throws SQLException {
		if (reservationState == null) {
			preparedStatement.setNull(i, jdbcType.TYPE_CODE);
		} else {
			preparedStatement.setString(i, reservationState.name());
		}
	}

	@Override
	public ReservationState getResult(ResultSet resultSet, String s) throws SQLException {
		return EnumUtils.isValidEnum(ReservationState.class, resultSet.getString(s)) ? ReservationState.valueOf(resultSet.getString(s)) : null;
	}

	@Override
	public ReservationState getResult(ResultSet resultSet, int i) throws SQLException {
		return EnumUtils.isValidEnum(ReservationState.class, resultSet.getString(i)) ? ReservationState.valueOf(resultSet.getString(i)) : null;
	}

	@Override
	public ReservationState getResult(CallableStatement callableStatement, int i) throws SQLException {
		return EnumUtils.isValidEnum(ReservationState.class, callableStatement.getString(i)) ? ReservationState.valueOf(callableStatement.getString(i)) : null;
	}
}