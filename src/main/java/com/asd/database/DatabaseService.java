package com.asd.database;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;

public class DatabaseService {

	private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);

	private static SqlSessionFactory sqlMapper = null;

	private static SqlSessionFactory initializeSqlMapper(String dbEnvironmentId, String environmentId) {
		SqlSessionFactory sqlSessionFactory = null;
		try {
			String resource = "config/Configuration.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, dbEnvironmentId);
			LOG.debug("Using Configuration: " + environmentId);
			LOG.debug("openSession " + sqlSessionFactory);
		} catch (Exception x) {
			LOG.error(x.getMessage());
			x.printStackTrace();
		}
		return sqlSessionFactory;
	}

	public static SqlSession openSession() {
		if (sqlMapper == null) {
			sqlMapper = initializeSqlMapper("testMappers", "testMappers");
		}
		return sqlMapper.openSession();
	}

}
