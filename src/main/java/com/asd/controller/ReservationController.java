package com.asd.controller;

import com.asd.dto.ReservationDto;
import com.asd.services.implementation.ReservationService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/reservation")
@Api(tags = { "Reservation API controller" }, description = "Contains CRUD operations")
public class ReservationController {
	private final ReservationService service;

	public ReservationController(ReservationService inService) {
		service = inService;
	}

	@PostMapping("")
	@ApiOperation(value = "Create", notes = "Create reservation entity. Id and version fields will be ignored and replaced with correct ones.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid object to create") })
	public ResponseEntity<?> create(@RequestBody @ApiParam(name = "dtoToCreate", value = "NOT NULL") ReservationDto reservationDto) {
		service.create(reservationDto);
		return ResponseEntity.ok()
				.build();
	}

	@PutMapping("")
	@ApiOperation(value = "Update", notes = "Update reservation entity. Version field will be ignored and replaced with correct one.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid object to update") })
	public ResponseEntity<?> update(@RequestBody @ApiParam(name = "dtoToUpdate", value = "NOT NULL") ReservationDto reservationDto) {
		service.update(reservationDto);
		return ResponseEntity.ok()
				.build();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Read by id", notes = "Reading reservation, returns reservation by id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid object id to read") })
	public ResponseEntity<ReservationDto> readById(@PathVariable(name = "id") @ApiParam(value = "id for reservation reading", example = "123") Integer id) {
		return ResponseEntity.ok(service.readById(id));
	}

	@PutMapping("/{id}")
	@ApiOperation(value = "Change state", notes = "State changing by given id. State can be null")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid state to change or object id") })
	public ResponseEntity<?> changeState(@PathVariable @ApiParam(value = "id for changing state", example = "123") Integer id,
			@RequestParam(name = "state") @ApiParam(allowableValues = "null, Canceled, Closed, Confirmed, Failed, FullyPaid, Initial, Provisional") String state) {
		service.changeState(id, state);
		return ResponseEntity.ok()
				.build();
	}

	@PutMapping("/change-to-closed/{id}")
	@ApiOperation(value = "Change state to closed", notes = "State changing to closed by given id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid object id for changing its state to closed") })
	public ResponseEntity<?> changeStateToClosed(@PathVariable @ApiParam(value = "id for changing state to closed", example = "123") Integer id) {
		service.changeStateToClosed(id);
		return ResponseEntity.ok()
				.build();
	}

	@PutMapping("/list")
	@ApiOperation(value = "Insert list", notes = "Insert list of reservations. List should contains not null items only")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid objects to insert") })
	public ResponseEntity<?> insertList(@RequestBody @ApiParam(value = "list to insert") List<ReservationDto> reservationDtoList) {
		service.insertList(reservationDtoList);
		return ResponseEntity.ok()
				.build();
	}

	@GetMapping("/greater-than")
	@ApiOperation(value = "Find greater than date and quote", notes = "searching for items that has fromDate and quote properties greater than given ones")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid comparison objects (from date, quote) or limit") })
	public ResponseEntity<List<ReservationDto>> findGreaterThanDateAndQuote(
			@RequestParam(name = "from_date", required = false) @ApiParam(value = "lower datetime limit") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime fromDate,
			@RequestParam(name = "quote", required = false) @ApiParam(value = "lower quote limit", example = "123") Integer quote,
			@RequestParam(name = "limit", required = false) @ApiParam(value = "count of items", example = "123") Integer limit) {
		return ResponseEntity.ok(service.findGreaterThanDateAndQuote(fromDate, quote, limit));
	}

	@PutMapping("/state/{state}")
	@ApiOperation(value = "Update state of items", notes = "updating items state by product id (not required) and time range (not required too)")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid filters (product id, date range) or state") })
	public ResponseEntity<?> updateState(
			@RequestParam(name = "product_id", required = false) @ApiParam(value = "product id for state updating", example = "123") Integer productId,
			@RequestParam(name = "from_date", required = false) @ApiParam(value = "lower limit of datetime range") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime fromDate,
			@RequestParam(name = "to_date", required = false) @ApiParam(value = "upper limit of datetime range") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime toDate,
			@PathVariable(name = "state") @ApiParam(allowableValues = "null, Canceled, Closed, Confirmed, Failed, FullyPaid, Initial, Provisional") String state) {
		service.updateState(productId, fromDate, toDate, state);
		return ResponseEntity.ok()
				.build();
	}

	@GetMapping("/customer/{customer_id}")
	@ApiOperation(value = "Read by customer id", notes = "find items by customer id and items sample params (offset and limit)")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid customer id or pagination params (start, limit)") })
	public ResponseEntity<List<ReservationDto>> readByCustomerId(
			@PathVariable(name = "customer_id") @ApiParam(value = "not NULL", example = "123") Integer customerId,
			@RequestParam(name = "start", required = false) @ApiParam(value = "items offset", example = "123") Integer start,
			@RequestParam(name = "limit", required = false) @ApiParam(value = "count of selected items", example = "123") Integer limit) {
		return ResponseEntity.ok(service.readByCustomerId(customerId, start, limit));
	}

	@GetMapping("/date-range")
	@ApiOperation(value = "Read by date range", notes = "reading items by date range (start date is included and end date is excluded)")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid date range") })
	public ResponseEntity<List<ReservationDto>> readByDateRange(
			@RequestParam(name = "start_date") @ApiParam(value = "lower limit of date range (included); not NULL") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime startDate,
			@RequestParam(name = "end_date") @ApiParam(value = "upper limit of date range (excluded); not NULL") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime endDate) {
		return ResponseEntity.ok(service.readByDateRange(startDate, endDate));
	}

	@GetMapping("/confirmation/{pattern}")
	@ApiOperation(value = "Get by confirmation id pattern", notes = "searching for items that has the confirmation matches to pattern")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid confirmation id pattern") })
	public ResponseEntity<?> getByConfirmationIdPattern(@PathVariable(name = "pattern") @ApiParam(value = "not NULL") String pattern) {
		return ResponseEntity.ok(service.findByConfirmationIdPattern(pattern));
	}

	@GetMapping("/archive-price")
	@ApiOperation(value = "Get archive prices", notes = "searching for archive prices that corresponds reservation id (not required)")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"), @ApiResponse(code = 500, message = "Invalid reservation id or limit") })
	public ResponseEntity<?> getArchivePrices(
			@RequestParam(name = "reservation_id", required = false) @ApiParam(value = "reservation id for getting archive prices", example = "123") Integer reservationId,
			@RequestParam(name = "limit", required = false) @ApiParam(value = "count of selected items", example = "123") Integer limit) {
		return ResponseEntity.ok(service.getReservationAndGroupArchivePriceByTypeAndSortByVersion(reservationId, limit));
	}
}