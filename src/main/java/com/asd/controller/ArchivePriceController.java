package com.asd.controller;

import com.asd.dto.ArchivePriceDto;
import com.asd.services.IsArchivePriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/archive-price")
@Api(tags = { "Archive price API controller" }, description = "Contains some CRUD operations")
public class ArchivePriceController {
	private final IsArchivePriceService service;

	@GetMapping("/{id}")
	@ApiOperation(value = "Read by id", notes = "Reading archive price, returns archive price by id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid id(null) or troubles in server(check the message)") })
	public ResponseEntity<ArchivePriceDto> getById(@PathVariable("id") @ApiParam(value = "Id for searching archive price", example = "123") Integer id) {
		return new ResponseEntity<>(service.read(id), HttpStatus.OK);
	}

	@PostMapping("")
	@ApiOperation(value = "Create", notes = "Create archive price. Get not null archive price for create")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid archive price(null) or troubles in server(check the message)") })
	public ResponseEntity<String> create(
			@RequestBody @ApiParam(name = "dtoForCreate", value = "EntityType field - name of related table, entityId - id from this table."
					+ " Version and id created automatically, not required") ArchivePriceDto dto) {
		service.create(dto);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	@PutMapping("")
	@ApiOperation(value = "Update", notes = "Update archive price. Get not null archive price for update")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid archive price(null) or troubles in server(check the message)") })
	public ResponseEntity<String> update(@RequestBody @ApiParam(name = "dtoForUpdate", value = "EntityType field - name of related table, "
			+ "entityId - id from this table. Version update automatically, not required") ArchivePriceDto dto) {
		service.update(dto);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	@PostMapping("/list")
	@ApiOperation(value = "Insert list", notes = "Insert list of archive price. List must contains only not null archive prices")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid archive price(null), invalid list(empty or null) or troubles in server(check the message)") })
	public ResponseEntity<String> insertList(@RequestBody @ApiParam(value = "List for inserting") List<ArchivePriceDto> dtoList) {
		service.insertList(dtoList);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	@PutMapping("/{id}")
	@ApiOperation(value = "Change state", notes = "Changing state by selected id.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid id(null) or state(check the allowable values) or troubles in server(check the message)") })
	public ResponseEntity<String> changeState(@PathVariable @ApiParam(value = "Id for changing state", example = "123") Integer id,
			@ApiParam(value = " State can be null", allowableValues = "null, Created, Final") @RequestParam String state) {
		service.changeState(id, state);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}
}