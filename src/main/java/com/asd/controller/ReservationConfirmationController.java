package com.asd.controller;

import com.asd.dto.ReservationConfirmationDto;
import com.asd.services.IsReservationConfirmationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/reservation-confirmation")
@Api(tags = { "Reservation confirmation API controller" }, description = "Contains some CRUD operations")
public class ReservationConfirmationController {
	private final IsReservationConfirmationService service;

	public ReservationConfirmationController(IsReservationConfirmationService inService) {
		service = inService;
	}

	@PostMapping("")
	@ApiOperation(value = "Create", notes = "Creating reservation confirmation.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid entity(NULL), reservation id = NULL or troubles in server(check the message)") })
	public ResponseEntity<?> create(
			@RequestBody @ApiParam(name = "reservationConfirmationForCreate", value = "Reservation id must be NOT NULL. Version, id and "
					+ "createdTime created automatically, not required") ReservationConfirmationDto resConfirmDto) {
		service.create(resConfirmDto);
		return ResponseEntity.ok()
				.build();
	}

	@PutMapping("")
	@ApiOperation(value = "Update", notes = "Updating reservation confirmation.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid entity(NULL), reservation id = NULL or troubles in server(check the message)") })
	public ResponseEntity<?> update(
			@RequestBody @ApiParam(name = "reservationConfirmationForUpdate", value = " Reservation id must be NOT NULL. Version updated"
					+ " automatically, not required") ReservationConfirmationDto resConfirmDto) {
		service.update(resConfirmDto);
		return ResponseEntity.ok()
				.build();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Read by id", notes = "Reading reservation confirmation, returns reservation confirmation by id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid id(null) or troubles in server(check the message)") })
	public ResponseEntity<ReservationConfirmationDto> read(
			@PathVariable @ApiParam(value = "Id for searching reservation confirmation", example = "123") Integer id) {
		return ResponseEntity.ok(service.readById(id));
	}

	@PutMapping("/list")
	@ApiOperation(value = "Insert list", notes = "Insert list of archive price.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful"),
			@ApiResponse(code = 500, message = "Invalid archive price(null), invalid list(empty or null) or troubles in server(check the message)") })
	public ResponseEntity<?> insertList(@RequestBody @ApiParam(value = "List for inserting. List must contains only NOT NULL values"
			+ " reservation confirmations") List<ReservationConfirmationDto> dtoList) {
		service.insertList(dtoList);
		return ResponseEntity.ok()
				.build();
	}
}