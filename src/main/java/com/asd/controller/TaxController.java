package com.asd.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tax")
public class TaxController {

	@GetMapping("/test")
	public ResponseEntity<String> test() {
		return new ResponseEntity<>("200 OK", HttpStatus.OK);
	}
}
