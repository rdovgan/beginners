package com.asd.mapper;

import com.asd.constants.ArchivePriceState;
import com.asd.entities.ArchivePrice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArchivePriceMapper {

	void create(ArchivePrice priceToCreate);

	ArchivePrice read(Integer id);

	void update(ArchivePrice price);

	void changeState(Integer id, ArchivePriceState state);

	void insertList(@Param("items") List<ArchivePrice> itemsToInsert);

	List<ArchivePrice> groupByTypeAndSortedByVersion(@Param("entityType") String entityType, @Param("entityId") Integer entityId,
			@Param("limit") Integer limit);
}
