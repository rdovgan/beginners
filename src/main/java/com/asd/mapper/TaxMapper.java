package com.asd.mapper;

import com.asd.entities.IsTax;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface TaxMapper {

	void create(IsTax taxToCreate);

	IsTax readById(Integer id);

	void update(IsTax tax);

	List<IsTax> readByProductId(Integer productId);

	List<IsTax> readByProductIdAndState(@Param("productId") Integer productId, @Param("state") String state);

	void changeTaxStateToFinal(String productId);

	List<IsTax> getTaxByIds(@Param("ids") List<Integer> taxIds);

	void insertList(@Param("items") List<IsTax> itemsToInsert);

	void updateList(@Param("items") List<IsTax> itemsToUpdate);

}
