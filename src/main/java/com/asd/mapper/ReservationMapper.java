package com.asd.mapper;

import com.asd.constants.ReservationState;
import com.asd.entities.Reservation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservationMapper {
	void create(Reservation reservation);

	void update(Reservation reservation);

	Reservation readById(Integer id);

	void changeState(@Param("id") Integer id, @Param("state") ReservationState state);

	void changeStateToClosed(Integer id);

	void insertList(@Param("reservationsForInsert") List<Reservation> reservationList);

	List<Reservation> findGreaterThanDateAndQuote(LocalDateTime fromDate, Integer quote, Integer limit);

	void updateState(Integer productId, LocalDateTime fromDate, LocalDateTime toDate, ReservationState state);

	List<Reservation> readByCustomerId(Integer customerId, Integer start, Integer limit);

	List<Reservation> readByDateRange(LocalDateTime startDate, LocalDateTime endDate);

	List<Reservation> findByConfirmationIdPattern(String pattern);
}