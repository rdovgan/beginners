package com.asd.mapper;

import com.asd.entities.ReservationConfirmation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationConfirmationMapper {
	void create(ReservationConfirmation reservationConfirmation);

	void update(ReservationConfirmation reservationConfirmation);

	ReservationConfirmation readById(Integer id);

	void insertList(@Param("confirmations") List<ReservationConfirmation> reservationConfirmationList);
}
