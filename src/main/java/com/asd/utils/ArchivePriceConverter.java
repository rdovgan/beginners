package com.asd.utils;

import com.asd.dto.ArchivePriceDto;
import com.asd.entities.ArchivePrice;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ArchivePriceConverter {
	public ArchivePrice toEntity(ArchivePriceDto dto) {
		if (dto == null) {
			return null;
		}
		return ArchivePrice.builder()
				.id(dto.getId())
				.entityId(dto.getEntityId())
				.name(dto.getName())
				.entityType(dto.getEntityType())
				.state(dto.getState())
				.type(dto.getType())
				.value(dto.getValue())
				.version(dto.getVersion())
				.build();
	}

	public List<ArchivePrice> toEntity(List<ArchivePriceDto> dtoList) {
		return dtoList.stream()
				.map(this::toEntity)
				.collect(Collectors.toList());
	}

	public ArchivePriceDto toDto(ArchivePrice entity) {
		if (entity == null) {
			return null;
		}
		return ArchivePriceDto.builder()
				.id(entity.getId())
				.entityId(entity.getEntityId())
				.name(entity.getName())
				.entityType(entity.getEntityType())
				.state(entity.getState())
				.type(entity.getType())
				.value(entity.getValue())
				.version(entity.getVersion())
				.build();
	}

	public List<ArchivePriceDto> toDto(List<ArchivePrice> entityList) {
		return entityList.stream()
				.map(this::toDto)
				.collect(Collectors.toList());
	}
}