package com.asd.utils;

import com.asd.dto.ReservationDto;
import com.asd.entities.Reservation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReservationConverter {
	public ReservationDto convertToDto(Reservation reservation) {
		if (reservation == null) {
			return null;
		}
		return ReservationDto.builder()
				.id(reservation.getId())
				.organizationId(reservation.getOrganizationId())
				.agentId(reservation.getAgentId())
				.customerId(reservation.getCustomerId())
				.productId(reservation.getProductId())
				.state(reservation.getState())
				.fromDate(reservation.getFromDate())
				.toDate(reservation.getToDate())
				.price(reservation.getPrice())
				.quote(reservation.getQuote())
				.currency(reservation.getCurrency())
				.guestNumber(reservation.getGuestNumber())
				.notes(reservation.getNotes())
				.version(reservation.getVersion())
				.build();
	}

	public Reservation convertToEntity(ReservationDto reservationDto) {
		if (reservationDto == null) {
			return null;
		}

		return Reservation.builder()
				.id(reservationDto.getId())
				.organizationId(reservationDto.getOrganizationId())
				.agentId(reservationDto.getAgentId())
				.customerId(reservationDto.getCustomerId())
				.productId(reservationDto.getProductId())
				.state(reservationDto.getState())
				.fromDate(reservationDto.getFromDate())
				.toDate(reservationDto.getToDate())
				.price(reservationDto.getPrice())
				.quote(reservationDto.getQuote())
				.currency(reservationDto.getCurrency())
				.guestNumber(reservationDto.getGuestNumber())
				.notes(reservationDto.getNotes())
				.version(reservationDto.getVersion())
				.build();
	}

	public List<ReservationDto> convertToDtoList(List<Reservation> reservationList) {
		if (reservationList == null) {
			return null;
		}
		return reservationList.stream()
				.map(this::convertToDto)
				.collect(Collectors.toList());
	}

	public List<Reservation> convertToEntityList(List<ReservationDto> reservationDtoList) {
		if (reservationDtoList == null) {
			return null;
		}
		return reservationDtoList.stream()
				.map(this::convertToEntity)
				.collect(Collectors.toList());
	}
}
