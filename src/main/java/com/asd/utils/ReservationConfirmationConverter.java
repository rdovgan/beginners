package com.asd.utils;

import com.asd.dto.ReservationConfirmationDto;
import com.asd.entities.ReservationConfirmation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReservationConfirmationConverter {
	public ReservationConfirmation toEntity(ReservationConfirmationDto dto) {
		if (dto == null) {
			return null;
		}
		return ReservationConfirmation.builder()
				.id(dto.getId())
				.confirmationId(dto.getConfirmationId())
				.reservationId(dto.getReservationId())
				.channelPartnerId(dto.getChannelPartnerId())
				.createdTime(dto.getCreatedTime())
				.version(dto.getVersion())
				.build();
	}

	public ReservationConfirmationDto toDto(ReservationConfirmation entity) {
		if (entity == null) {
			return null;
		}
		return ReservationConfirmationDto.builder()
				.id(entity.getId())
				.confirmationId(entity.getConfirmationId())
				.reservationId(entity.getReservationId())
				.channelPartnerId(entity.getChannelPartnerId())
				.createdTime(entity.getCreatedTime())
				.version(entity.getVersion())
				.build();
	}

	public List<ReservationConfirmation> toEntity(List<ReservationConfirmationDto> dtoList) {
		return dtoList.stream()
				.map(this::toEntity)
				.collect(Collectors.toList());
	}

	public List<ReservationConfirmationDto> toDto(List<ReservationConfirmation> entityList) {
		return entityList.stream()
				.map(this::toDto)
				.collect(Collectors.toList());
	}
}