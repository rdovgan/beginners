<!DOCTYPE HTML>
<html lang="EN">
<head>
	<meta charset="UTF-8" />
	<title>Welcome</title>
</head>
	<body>
	<#if data.reservation??>
		<h2>Reservation info: </h2>
		<p>
			<b>ID:</b>  ${data.reservation.id!}
		</p>
		<p>
			<b>Organization id:</b>  ${data.reservation.organizationId!}
		</p>
		<p>
			<b>Agent id:</b>  ${data.reservation.agentId!}
		</p>
		<p>
			<b>Product id:</b>  ${data.reservation.productId!}
		</p>
		<p>
			<b>State:</b>  ${data.reservation.state!}
		</p>
		<p>
			<b>From date:</b>  ${data.reservation.fromDate!}
		</p>
		<p>
			<b>To date:</b>  ${data.reservation.toDate!}
		</p>
		<p>
			<b>Price:</b>  ${data.reservation.price!}
		</p>
		<p>
            <#setting number_format="#.##">
			<b>Quote:</b>  ${data.reservation.quote!}
		</p>
		<p>
			<b>Currency:</b>  ${data.reservation.currency!}
		</p>
		<p>
			<b>Guest number:</b>  ${data.reservation.guestNumber!}
		</p>
		<p>
			<b>Notes:</b>  ${data.reservation.notes!}
		</p>
		<p>
			<b>Version:</b>  ${data.reservation.version!}
		</p>
	</#if>

	<h2>Related archive prices:</h2>
	<#list data.typePriceMap as key, values>
		<h4>${key}: </h4>
		<#list values as val>
			${val}<#sep>,  </br></#sep>
		</#list> </br>
	</#list>

	</body>
</html>